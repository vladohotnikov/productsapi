﻿using Core;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure;

public class DataBaseContext : DbContext
{
    public DbSet<Product> Products { get; set; } = null!;

    public DataBaseContext(DbContextOptions dbContextOptions)
        : base(dbContextOptions) { }
}
