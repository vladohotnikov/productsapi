﻿using System;
using Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure;

public static class ServiceRegistration
{
    public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<DataBaseContext>(options =>
        {
            options.UseSqlite(configuration.GetConnectionString("SQLLiteDb"));
        });

        services.AddScoped<IProductsRepository, ProductsRepository>();

        return services;
    }
}
