﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure;

public class ProductsRepository : IProductsRepository
{
    private readonly DataBaseContext _context;
    private bool _disposed;

    public ProductsRepository(DataBaseContext context)
    {
        _context = context;
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!_disposed)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

        _disposed = true;
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    /// <inheritdoc />
    public async Task<Product?> Get(long id, CancellationToken cancellationToken = default)
    {
        return await _context.Products
            .AsNoTracking()
            .FirstOrDefaultAsync(p => p.Id == id, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<long> Create(Product product, CancellationToken cancellationToken = default)
    {
        await _context.Products.AddAsync(product, cancellationToken);
        await _context.SaveChangesAsync(cancellationToken);

        var productFromDb = await Get(product.Id, cancellationToken);
        if (productFromDb == null)
        {
            throw new Exception("Error while saving product");
        }

        return productFromDb.Id;
    }

    /// <inheritdoc />
    public Task Update(Product product, CancellationToken cancellationToken = default)
    {
        _context.Products.Update(product);
        return _context.SaveChangesAsync(cancellationToken);
    }

    /// <inheritdoc />
    public async Task Delete(long id, CancellationToken cancellationToken = default)
    {
        var productFromDb = await _context.Products.FirstOrDefaultAsync(p => p.Id == id, cancellationToken: cancellationToken);
        if (productFromDb == null)
        {
            throw new Exception("Error while delete product");
        }
        _context.Products.Remove(productFromDb);
        await _context.SaveChangesAsync(cancellationToken);
    }

    /// <inheritdoc />
    public Task<List<Product>> GetList(CancellationToken cancellationToken = default)
    {
        return _context.Products
            .AsNoTracking()
            .ToListAsync(cancellationToken);
    }
}
