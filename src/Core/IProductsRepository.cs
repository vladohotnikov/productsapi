﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Core;

public interface IProductsRepository : IDisposable
{
    Task<Product?> Get(long id, CancellationToken cancellationToken = default);
    Task<long> Create(Product product, CancellationToken cancellationToken = default);
    Task Update(Product product, CancellationToken cancellationToken = default);
    Task Delete(long id, CancellationToken cancellationToken = default);
    Task<List<Product>> GetList(CancellationToken cancellationToken = default);
}
