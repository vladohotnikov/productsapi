﻿using System.ComponentModel.DataAnnotations;

namespace Core;

/// <summary>
/// Product information.
/// </summary>
public class Product
{
    /// <summary>
    /// Unique identifier.
    /// </summary>
    [Key]
    public long Id { get; set; }

    /// <summary>
    /// String information about product.
    /// </summary>
    public string Comment { get; set; } = string.Empty;
}
