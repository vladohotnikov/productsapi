﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OpenTelemetry.Logs;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;
using WebAPI.Configuration;

namespace WebAPI.Extensions;

public static class ServiceRegistration
{
    public static IServiceCollection AddWebAPIServices(this IServiceCollection services, IConfiguration configuration)
    {
        var observabilitySection = configuration.GetSection(ObservabilityOptions.OptionsKey);

        if (!observabilitySection.Exists())
        {
            throw new Exception("Impossible to configure observability without .json configuration section");
        }

        var observabilityOptions = observabilitySection.Get<ObservabilityOptions>();

        services.AddObservability(observabilityOptions);

        return services;
    }

    private static IServiceCollection AddObservability(this IServiceCollection services, ObservabilityOptions observabilityOptions)
    {
        const string serviceName = "ProductWebAPI";
        const string serviceVersion = "1.0.0";

        services.AddLogging(builder =>
        {
            if (!observabilityOptions.Logging.IsOtherLoggingProvidersEnabled)
            {
                builder.ClearProviders();
            }

            if (observabilityOptions.Logging.IsEnabled)
            {
                builder
                    .AddOpenTelemetry(options =>
                    {
                        options.ParseStateValues = true;
                        options.IncludeFormattedMessage = true;
                        options.AddOtlpExporter(conf => { conf.Endpoint = observabilityOptions.OpenTelemetryCollectorUri; });

                        if (observabilityOptions.Logging.IsConsoleExporterEnabled)
                        {
                            options.AddConsoleExporter();
                        }
                    });
            }
        });

        services.AddOpenTelemetryTracing(builder =>
        {
            builder
                .AddOtlpExporter(conf =>
                {
                    conf.Endpoint = observabilityOptions.OpenTelemetryCollectorUri;
                })
                .AddConsoleExporter()
                .AddSource(serviceName)
                .SetResourceBuilder(ResourceBuilder.CreateDefault().AddService(serviceName: serviceName, serviceVersion: serviceVersion))
                .AddHttpClientInstrumentation()
                .AddAspNetCoreInstrumentation();
        });

        return services;
    }
}
