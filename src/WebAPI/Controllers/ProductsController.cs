﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers;

/// <summary>
/// Products controller.
/// </summary>
[Route("products")]
public class ProductsController : ControllerBase
{
    private readonly IProductsRepository _productsRepository;

    public ProductsController(IProductsRepository productsRepository)
    {
        _productsRepository = productsRepository;
    }

    /// <summary>
    /// Get existing product.
    /// </summary>
    /// <param name="id">Unique identifier of product.</param>
    /// <param name="cancellationToken">Token for cancelling operations.</param>
    /// <returns></returns>
    [HttpGet("{id:long}")]
    public async Task<Product?> Get(long id, CancellationToken cancellationToken = default)
    {
        return await _productsRepository.Get(id, cancellationToken);
    }

    /// <summary>
    /// Get all products.
    /// </summary>
    /// <param name="cancellationToken">Token for cancelling operations.</param>
    /// <returns></returns>
    [HttpGet()]
    public async Task<List<Product>> GetList(CancellationToken cancellationToken = default)
    {
        return await _productsRepository.GetList(cancellationToken);
    }

    /// <summary>
    /// Create new product.
    /// </summary>
    /// <param name="product">Product information.</param>
    /// <param name="cancellationToken">Token for cancelling operations.</param>
    /// <returns></returns>
    [HttpPost("")]
    public async Task<long> Create(Product product, CancellationToken cancellationToken = default)
    {
        return await _productsRepository.Create(product, cancellationToken);
    }

    /// <summary>
    /// Update product.
    /// </summary>
    /// <param name="product">Product information.</param>
    /// <param name="cancellationToken">Token for cancelling operations.</param>
    /// <returns></returns>
    [HttpPatch("")]
    public async Task Update(Product product, CancellationToken cancellationToken = default)
    {
        await _productsRepository.Update(product, cancellationToken);
    }

    /// <summary>
    /// Delete product.
    /// </summary>
    /// <param name="id">Unique identifier of product.</param>
    /// <param name="cancellationToken">Token for cancelling operations.</param>
    /// <returns></returns>
    [HttpDelete("{id:long}")]
    public async Task Delete(long id, CancellationToken cancellationToken = default)
    {
        await _productsRepository.Delete(id, cancellationToken);
    }
}
