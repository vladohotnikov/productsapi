﻿using System;

namespace WebAPI.Configuration;

public class ObservabilityOptions
{
    public static string OptionsKey = "Observability";
    public Uri OpenTelemetryCollectorUri { get; set; } = null!;

    public LoggingSettings Logging { get; set; } = null!;
}
