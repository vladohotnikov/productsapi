﻿namespace WebAPI.Configuration;

public class LoggingSettings
{
    public bool IsEnabled { get; set; }

    public bool IsConsoleExporterEnabled { get; set; }

    public bool IsOtherLoggingProvidersEnabled { get; set; }
}
