﻿using System;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using RestEase;

namespace ProductsAPIClient;

public static class ProductsClientFactory
{
    public static IProductsClient Create(Uri uri)
    {
        var restClient = new RestClient(uri)
        {
            JsonSerializerSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                Converters = { new StringEnumConverter() },
            },
        }.For<IProductsClient>();

        return restClient;
    }

    public static IProductsClient Create(string uri) => Create(new Uri(uri));

    public static IProductsClient Create(HttpClient client)
    {
        var restClient = new RestClient(client)
        {
            JsonSerializerSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                Converters = { new StringEnumConverter() },
            },
        }.For<IProductsClient>();

        return restClient;
    }
}
