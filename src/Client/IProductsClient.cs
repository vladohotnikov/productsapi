﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Core;
using RestEase;

namespace ProductsAPIClient;

[BasePath("products")]
public interface IProductsClient : IDisposable
{
    [Get("{id}")]
    Task<Product?> Get([Path] long id, CancellationToken cancellationToken = default);

    [Get()]
    Task<List<Product>> GetList(CancellationToken cancellationToken = default);

    [Post("")]
    Task<long> Create([Body] Product product, CancellationToken cancellationToken = default);

    [Patch("")]
    Task Update([Body] Product product, CancellationToken cancellationToken = default);

    [Delete("{id}")]
    Task Delete([Path] long id, CancellationToken cancellationToken = default);
}
