﻿using Core;
using FluentAssertions;
using Moq;
using WebAPI.Controllers;

namespace WebApiTests.Controllers
{
    public class ProductControllerTests
    {
        [Fact]
        public void Get_WithProductId_ReturnSameId()
        {
            // arrange
            var repositoryMock = new Mock<IProductsRepository>();
            var controller = new ProductsController(repositoryMock.Object);
            var productId = 1;
            var product = new Product { Id = productId };

            repositoryMock
                .Setup(r => r.Get(It.IsAny<long>(), default))
                .ReturnsAsync(product);

            // act
            var result = controller.Get(productId);

            // assert
            result.Id.Should().Be(productId);
        }
    }
}
