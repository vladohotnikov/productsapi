﻿using System;
using FluentAssertions;
using ProductsAPIClient;

namespace ProductsAPIClientTests;

public class ProductsFactoryTests
{
    [Fact]
    public void CreateClient_WithCorrectInterface_ShouldNotThrow()
    {
        // arrange & act
        Func<IProductsClient> createClientAction = () => ProductsClientFactory.Create(new Uri("http://localhost"));

        // assert
        createClientAction.Should().NotThrow();
    }
}
