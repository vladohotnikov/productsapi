# ProductsAPI
> An assesment.

User Story:
As an API user, I want to manage products via API so that my department can create, update and
delete products via Postman.

Acceptance Criteria:
1. Be able to create, update and delete products;
1. A product cannot be duplicated;
1. The data should be persisted for a future search;
1. API should be documented;

Suggestions:
1. Backend should be done preferably with .NET Core, but other languages and frameworks are welcome;
1. The code should be published in github or gitlab.

## Installing / Getting started

No special information.
DataBase file will create automaticaly in `db` folder.

### Initial Configuration

No special information.

## Developing

Use Visual Studio.

### Building

Use Visual Studio.

## Configuration

#### ConnectionStrings:SQLLiteDb
Type: `String`  
Default:

Set SQLLite connection string. 

## Licensing

MIT License

Copyright (c) [2022] [VladimirOkhotnikov]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.